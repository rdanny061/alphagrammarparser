// Generated from C:/Users/rdann/OneDrive - Estudiantes ITCR/TEC/6 Semestre Ingeniería en Computación/Compiladores e Intérpretes/Tarea2/MiParser\miParser.g4 by ANTLR 4.8
package generated;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link miParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface miParserVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code programAST}
	 * labeled alternative in {@link miParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgramAST(miParser.ProgramASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code commandAST}
	 * labeled alternative in {@link miParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCommandAST(miParser.CommandASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assignSCAST}
	 * labeled alternative in {@link miParser#singleCommand}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignSCAST(miParser.AssignSCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code callSCAST}
	 * labeled alternative in {@link miParser#singleCommand}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallSCAST(miParser.CallSCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifSCAST}
	 * labeled alternative in {@link miParser#singleCommand}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfSCAST(miParser.IfSCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code whileSCAST}
	 * labeled alternative in {@link miParser#singleCommand}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileSCAST(miParser.WhileSCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code letSCAST}
	 * labeled alternative in {@link miParser#singleCommand}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLetSCAST(miParser.LetSCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code beginSCAST}
	 * labeled alternative in {@link miParser#singleCommand}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBeginSCAST(miParser.BeginSCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code returnSCAST}
	 * labeled alternative in {@link miParser#singleCommand}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnSCAST(miParser.ReturnSCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code actualParamAST}
	 * labeled alternative in {@link miParser#actualParam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitActualParamAST(miParser.ActualParamASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code declarationAST}
	 * labeled alternative in {@link miParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclarationAST(miParser.DeclarationASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code constSDeclAST}
	 * labeled alternative in {@link miParser#singleDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstSDeclAST(miParser.ConstSDeclASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code varSDeclAST}
	 * labeled alternative in {@link miParser#singleDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarSDeclAST(miParser.VarSDeclASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code methodSDeclAST}
	 * labeled alternative in {@link miParser#singleDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodSDeclAST(miParser.MethodSDeclASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code varDeclAST}
	 * labeled alternative in {@link miParser#varDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarDeclAST(miParser.VarDeclASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code paramDeclsAST}
	 * labeled alternative in {@link miParser#paramDecls}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamDeclsAST(miParser.ParamDeclsASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typedenoterAST}
	 * labeled alternative in {@link miParser#typedenoter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypedenoterAST(miParser.TypedenoterASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expressionAST}
	 * labeled alternative in {@link miParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionAST(miParser.ExpressionASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numPEAST}
	 * labeled alternative in {@link miParser#primaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumPEAST(miParser.NumPEASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code identPEAST}
	 * labeled alternative in {@link miParser#primaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentPEAST(miParser.IdentPEASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code charPEAST}
	 * labeled alternative in {@link miParser#primaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharPEAST(miParser.CharPEASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code groupPEAST}
	 * labeled alternative in {@link miParser#primaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGroupPEAST(miParser.GroupPEASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code callPEAST}
	 * labeled alternative in {@link miParser#primaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallPEAST(miParser.CallPEASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code callFunctionAST}
	 * labeled alternative in {@link miParser#callFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallFunctionAST(miParser.CallFunctionASTContext ctx);
	/**
	 * Visit a parse tree produced by {@link miParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperator(miParser.OperatorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code identAST}
	 * labeled alternative in {@link miParser#ident}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentAST(miParser.IdentASTContext ctx);
}