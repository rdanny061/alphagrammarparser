parser grammar miParser;

options {
  tokenVocab = miScanner;
}

program             : singleCommand EOF                                     #programAST;
command             : singleCommand (PyCOMA singleCommand)*                 #commandAST;
singleCommand       : ident ASSIGN expression                               #assignSCAST
                    | callFunction                                          #callSCAST
                    | IF expression THEN singleCommand ELSE singleCommand   #ifSCAST
                    | WHILE expression DO singleCommand                     #whileSCAST
                    | LET declaration IN singleCommand                      #letSCAST
                    | BEGIN command END                                     #beginSCAST
                    | RETURN expression                                     #returnSCAST;
actualParam         : expression (PyCOMA expression)*                       #actualParamAST;
declaration         : singleDeclaration (PyCOMA singleDeclaration)*         #declarationAST;
singleDeclaration   : CONST IDENT VIR expression                            #constSDeclAST
    	            | varDeclaration                                        #varSDeclAST
                    | (typedenoter|VOID) IDENT PIZQ (paramDecls)? PDER singleCommand           #methodSDeclAST;

varDeclaration      : VAR IDENT DOSPUN typedenoter                          #varDeclAST;
paramDecls          : varDeclaration (PyCOMA varDeclaration)*               #paramDeclsAST;
typedenoter         : IDENT                                                 #typedenoterAST;
expression          : primaryExpression (operator primaryExpression)*       #expressionAST;
primaryExpression   : NUM                                                   #numPEAST
                    | ident                                                 #identPEAST
                    | CHAR                                                  #charPEAST
                    | PIZQ expression PDER                                  #groupPEAST
                    | callFunction                                          #callPEAST;
callFunction
locals [ParserRuleContext decl=null]
                    : ident PIZQ (actualParam)? PDER                        #callFunctionAST;
operator            : SUM | SUB | MUL | DIV                                 #operatorAST;

ident
locals [ParserRuleContext decl=null]
                    : IDENT                                                 #identAST;

//MODIFICAR ANÁLISIS CONTEXTUAL PARA QUE VERIFIQUE TIPOS Y ALCANCES EN LLAMADAS A MÉTODOS