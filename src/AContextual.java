import generated.miParser;
import generated.miParserBaseVisitor;
import org.antlr.v4.runtime.ParserRuleContext;

import java.util.ArrayList;

public class AContextual extends miParserBaseVisitor {
    private TablaSimbolos laTabla;
    private TablaSimbolos tablaMetodos;

    public AContextual() {
        this.laTabla = new TablaSimbolos();
        this.tablaMetodos = new TablaSimbolos();
        //la tabla de símbolos puede ser que no empiece vacía... Métodos predefinidos
    }

    @Override
    public Object visitProgramAST(miParser.ProgramASTContext ctx) {
        visit(ctx.singleCommand());
        return null;
    }

    @Override
    public Object visitCommandAST(miParser.CommandASTContext ctx) {
        visit(ctx.singleCommand(0));
        for (int i = 1; i < ctx.singleCommand().size(); i++) {
            visit(ctx.singleCommand(i));
        }
        return null;
    }

    @Override
    public Object visitAssignSCAST(miParser.AssignSCASTContext ctx) {
        //los tipos del ID y de la expresión deben concordar
        int tipoID = (int) visit(ctx.ident());
        int tipoExpr = (int) visit(ctx.expression());
        if (tipoID != tipoExpr)
            System.out.println("ERROR DE TIPOS: Tipos incompatibles para la asignación, entre " + printType(tipoID) + " y " + printType(tipoExpr));
        return null;
    }


    @Override
    public Object visitIfSCAST(miParser.IfSCASTContext ctx) {
        return super.visitIfSCAST(ctx);
    }

    @Override
    public Object visitWhileSCAST(miParser.WhileSCASTContext ctx) {
        return super.visitWhileSCAST(ctx);
    }

    @Override
    public Object visitLetSCAST(miParser.LetSCASTContext ctx) {
        laTabla.openScope();
        visit(ctx.declaration());
        visit(ctx.singleCommand());
        laTabla.closeScope();
        return null;
    }

    @Override
    public Object visitBeginSCAST(miParser.BeginSCASTContext ctx) {
        visit(ctx.command());
        return null;
    }

    @Override
    public Object visitDeclarationAST(miParser.DeclarationASTContext ctx) {
        visit(ctx.singleDeclaration(0));
        for (int i = 1; i < ctx.singleDeclaration().size(); i++) {
            visit(ctx.singleDeclaration(i));
        }
        return null;
    }

    @Override
    public Object visitConstSDeclAST(miParser.ConstSDeclASTContext ctx) {
        return super.visitConstSDeclAST(ctx);
    }

    @Override
    public Object visitVarDeclAST(miParser.VarDeclASTContext ctx) {
        //el int es 0 y el char es 1
        int tipo = -1;
        if (laTabla.buscar(ctx.IDENT().getText()) == null) {
            tipo = (int) visit(ctx.typedenoter());
            if (tipo != -1) {
                laTabla.insertar(ctx.IDENT().getSymbol(), tipo, ctx);
            } else
                System.out.println("ERROR DE TIPO: Tipo de datos para la declaración es inválido");
        } else
            System.out.println("ERROR DE ALCANCES: Ya existe declarado el identificador " + ctx.IDENT().getText());
        return tipo;
    }

    @Override
    public Object visitTypedenoterAST(miParser.TypedenoterASTContext ctx) {
        int res = -1;
        if (ctx.IDENT().getText().equals("int"))
            res = 0;
        else if (ctx.IDENT().getText().equals("char"))
            res = 1;
        return res;
    }

    @Override
    public Object visitExpressionAST(miParser.ExpressionASTContext ctx) {
        int tipo1 = (int) visit(ctx.primaryExpression(0));
        for (int i = 1; i < ctx.primaryExpression().size(); i++) {
            visit(ctx.operator(i - 1)); //se debe verificar la validez de los tipos según el operador
            int tipo2 = (int) visit(ctx.primaryExpression(i));
            if ((tipo1 != 0) || (tipo2 != 0)) {
                System.out.println("ERROR DE TIPOS: Tipos incompatibles entre " + printType(tipo1) + " y " + printType(tipo2));
                tipo1 = -1;
                break;
            }
        }
        return tipo1;
    }

    @Override
    public Object visitNumPEAST(miParser.NumPEASTContext ctx) {
        return 0;
    }

    @Override
    public Object visitCharPEAST(miParser.CharPEASTContext ctx) {
        return 1;
    }

    @Override
    public Object visitIdentAST(miParser.IdentASTContext ctx) {
        int tipo = -1;
        TablaSimbolos.Ident id = laTabla.buscar(ctx.IDENT().getText());
        if (id != null) {
            ctx.decl = id.declCtx;
            tipo = id.type;
        } else
            System.out.println("ERROR DE ALCANCES: El identificador " + ctx.IDENT().getText() + " no ha sido declarado!!!");
        return tipo;
    }

    @Override
    public Object visitCallPEAST(miParser.CallPEASTContext ctx) {
        return super.visitCallPEAST(ctx);
    }

    @Override
    public Object visitCallSCAST(miParser.CallSCASTContext ctx) {
        return super.visitCallSCAST(ctx);
    }

    @Override
    public Object visitCallFunctionAST(miParser.CallFunctionASTContext ctx) {
        TablaSimbolos.Ident id = tablaMetodos.buscar(ctx.ident().getText());
        int tipo = -1;
        if (id != null) {
            ctx.decl = id.declCtx;
            tipo = id.type;
            if (ctx.actualParam() != null) {
                visit(ctx.actualParam());
            }
        } else
            System.out.println("ERROR DE ALCANCES: El método " + ctx.ident().getText() + " no ha sido declarado!!!");
        return tipo;
    }

    @Override
    public Object visitActualParamAST(miParser.ActualParamASTContext ctx) {
        TablaSimbolos.Ident i = tablaMetodos.buscar(((miParser.CallFunctionASTContext) ctx.parent).ident().getText());

        if (ctx.expression().size() != i.getParams().size()) {
            System.out.println("ERROR DE CANTIDAD DE PARÁMETROS: El método " + i.tok.getText() + " debería tener " + i.getParams().size() + " parámetros " + "y recibió " + ctx.expression().size() + ".");
        } else {
            for (int j = 0; j < ctx.expression().size(); j++) {
                if (visit(ctx.expression().get(j)) != i.getParams().get(j)) {
                    System.out.println("ERROR DE TIPOS: Tipos incompatibles entre " + printType((int) visit(ctx.expression().get(j))) + " y " + printType(i.getParams().get(j)));
                }
            }
        }
        return null;
    }

    @Override
    public Object visitVarSDeclAST(miParser.VarSDeclASTContext ctx) {
        return super.visitVarSDeclAST(ctx);
    }

    @Override
    public Object visitMethodSDeclAST(miParser.MethodSDeclASTContext ctx) {
        //el int es 0 y el char es 1
        if (tablaMetodos.buscar(ctx.IDENT().getText()) == null) {
            int tipo = -1;
            if (ctx.typedenoter() == null)
                tipo = 2;
            else {
                tipo = (int) visit(ctx.typedenoter());
            }
            if (tipo != -1) {
                ArrayList<Integer> list = new ArrayList<Integer>();
                TablaSimbolos.Ident i = tablaMetodos.insertar(ctx.IDENT().getSymbol(), tipo, ctx);
                tablaMetodos.openScope();
                laTabla.openScope();
                if (ctx.paramDecls() != null) {
                    i.setParams((ArrayList<Integer>) visit(ctx.paramDecls()));
                }
                visit(ctx.singleCommand());
                laTabla.closeScope();
                tablaMetodos.closeScope();
            } else
                System.out.println("ERROR DE TIPO: Tipo de datos para la declaración es inválido");
        } else
            System.out.println("ERROR DE ALCANCES: Ya existe declarado el método " + ctx.IDENT().getText());
        return null;
    }

    @Override
    public Object visitReturnSCAST(miParser.ReturnSCASTContext ctx) {
        int tipo = (int) visit(ctx.expression());
        TablaSimbolos.Ident method;
        ParserRuleContext mtd = (ParserRuleContext) ctx.parent;
        while (!(mtd instanceof miParser.MethodSDeclASTContext)) {
            if (mtd.parent instanceof miParser.MethodSDeclASTContext) {
                method = tablaMetodos.buscar(((miParser.MethodSDeclASTContext) mtd.parent).IDENT().getText());
                if (tipo != method.type) {
                    System.out.println("ERROR DE TIPOS: El método " + method.tok.getText() + " es de tipo " + printType(method.type) + " y el tipo de retorno es " + printType(tipo) + ".");
                }
            }
            mtd = (ParserRuleContext) mtd.parent;
        }
        return null;
    }

    @Override
    public Object visitParamDeclsAST(miParser.ParamDeclsASTContext ctx) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add((int) visit(ctx.varDeclaration(0)));
        for (int i = 1; i < ctx.varDeclaration().size(); i++) {
            list.add((int) visit(ctx.varDeclaration(i)));
        }
        return list;
    }

    @Override
    public Object visitIdentPEAST(miParser.IdentPEASTContext ctx) {
        return visit(ctx.ident());
    }

    @Override
    public Object visitGroupPEAST(miParser.GroupPEASTContext ctx) {
        return visit(ctx.expression());
    }

    @Override
    public Object visitOperator(miParser.OperatorContext ctx) {
        return super.visitOperator(ctx);
    }

    public String printType(int type) {
        String t = "";
        if (type == 0) {
            t = "int";
        } else if (type == 1) {
            t = "char";
        } else if (type == 2) {
            t = "void";
        } else {
            t = "unknownType";
        }
        return t;
    }
}
