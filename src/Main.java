import generated.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;

import javax.swing.*;

/**
 * Created by oviquez on 23/4/2020.
 */
public class Main {
    public static void main(String[] args) {
        miScanner inst = null;
        miParser parser = null;
        ParseTree tree = null;

        CharStream input = null;
        CommonTokenStream tokens = null;
        MyErrorListener errorListener = null;
        try {
            input = CharStreams.fromFileName("test.txt");
            inst = new miScanner(input);
            tokens = new CommonTokenStream(inst);
            parser = new miParser(tokens);

            errorListener = new MyErrorListener();

            inst.removeErrorListeners();
            inst.addErrorListener(errorListener);

            parser.removeErrorListeners();
            parser.addErrorListener(errorListener);

            try {
                tree = parser.program();
                AContextual checkerVisitor = new AContextual();
                checkerVisitor.visit(tree);
            } catch (Exception e) {
                System.out.println("Error!!!");
                e.printStackTrace();
            }

            if (errorListener.hasErrors() == false) {
                System.out.println("Compilación Exitosa!!\n");
                //java.util.concurrent.Future<JFrame> treeGUI = org.antlr.v4.gui.Trees.inspect(tree, parser);
                //treeGUI.get().setVisible(true);
            } else {
                System.out.println("Compilación Fallida!!\n");
                System.out.println(errorListener.toString());
            }

        } catch (Exception e) {
            System.out.println("No hay archivo");
            e.printStackTrace();
        }

    }
}
