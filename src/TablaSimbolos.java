import generated.miParser;
import org.antlr.v4.runtime.*;

import java.util.ArrayList;
import java.util.LinkedList;

public class TablaSimbolos {
    LinkedList<Object> tabla;

    private int nivelActual;

    class Ident {
        Token tok;
        int type;
        int nivel;
        int valor;
        ParserRuleContext declCtx;
        ArrayList<Integer> paramsType;

        public Ident(Token t, int tp, ParserRuleContext decl) {
            tok = t;
            type = tp;
            nivel = nivelActual;
            valor = 0;
            declCtx = decl;
            paramsType = null;
        }

        public void setValue(int v) {
            valor = v;
        }

        public ArrayList<Integer> getParams() {
            return paramsType;
        }

        public void setParams(ArrayList<Integer> paramsType) {
            this.paramsType = paramsType;
        }
    }

    public TablaSimbolos() {
        tabla = new LinkedList<Object>();
        this.nivelActual = -1;
    }

    public Ident insertar(Token id, int tipo, ParserRuleContext decl) {
        Ident i = new Ident(id, tipo, decl);
        tabla.add(i);
        return i;
    }

    public Ident buscar(String nombre) {
        Ident temp = null;
        for (Object id : tabla)
            if (((Ident) id).tok.getText().equals(nombre))
                temp = ((Ident) id);
        return temp;
    }

    public void openScope() {
        nivelActual++;
    }

    public void closeScope() {
        tabla.removeIf(n -> (((Ident)n).nivel == nivelActual));
        nivelActual--;
    }

    /*public static LinkedList<Object> reverseLinkedList(LinkedList<Object> llist) {
        LinkedList<Object> revLinkedList = new LinkedList<Object>();
        for (int i = llist.size() - 1; i >= 0; i--) {
            revLinkedList.add(llist.get(i));
        }
        return revLinkedList;
    }*/

    public void imprimir() {
        System.out.println("----- INICIO TABLA ------");
        for (int i = 0; i < tabla.size(); i++) {
            Token s = (Token) ((Ident) tabla.get(i)).tok;
            System.out.println("Nombre: " + s.getText() + " - " + ((Ident) tabla.get(i)).nivel + " - " + ((Ident) tabla.get(i)).type);
/*            if (s.getType() == 0) System.out.println("\tTipo: Indefinido");
            else if (s.getType() == 1) System.out.println("\tTipo: Integer\n");
            else if (s.getType() == 2) System.out.println("\tTipo: String\n");*/
        }
        System.out.println("----- FIN TABLA ------");
    }
}
